Ansible Role: nftables
======================

Set up a simple dual-stack firewall on a Linux host using nftables.

The basic setup is good for simple network setups with multiple interfaces
in different zones, for example a simple laptop, a cloud VPS or
a workstation hosting some VMs.

Escape hatches for more advanced configuration are provided by incorporating
user templates which may add arbitrary nftables rules.

Requirements
------------

None.

Role Variables
--------------

Available variables are listed below, along with their default values (see `defaults/main.yml`):
```yaml
firewall:
  srcnat: False
  dstnat: False
  zones:
    wan:
      interfaces:
        - "{{ ansible_facts.default_ipv4.interface }}"
        - "{{ ansible_facts.default_ipv6.interface }}"
      input_dports:
        tcp:
          - ssh
```
`srcnat` enable/disable source NAT (masquerading).
`dstat` enable/disable destination NAT (redirection or port forwarding).
`zones` defines the names of the zones that the firewall knows about.
  `interfaces` contains a list of interfaces that belong to a zone. An interface
  can only be in a single zone at time, however this is not enforced by this role
  so the user must take of this themself.
  `input_dports` is a dictionary that can contain the keys `tcp` and `udp`. Under the respective keys follows a list of port numbers or service names as defined in `/etc/services`, traffic with this protocol/port combination will be allowed and added to the input chain.

The following configuration allows SSH on tcp/22 and mDNS on udp/5353
```yaml
firewall:
  srcnat: False
  dstnat: False
  zones:
    wan:
      interfaces:
        - "{{ ansible_facts.default_ipv4.interface }}"
        - "{{ ansible_facts.default_ipv6.interface }}"
      input_dports:
        tcp:
          - ssh
        udp:
          - mdns
```

Every zone can also be configured with a `redirect` dictionary which
allows configuration of DNAT (sometimes called port forwarding) for specific
protocol/port combinations. This is useful for running a rootless podman container that can not bind to the HTTP/HTTPS ports 80 and 443 respectively due to lack of root privileges.
In this case the container can be configured to listen on `localhost:8080` and `localhost:8443` and then incoming connections to tcp/80 and tcp/443 are DNATed accordingly:
```yaml
firewall:
  srcnat: False
  dstnat: True
  zones:
    wan:
      interfaces:
        - "{{ ansible_facts.default_ipv4.interface }}"
        - "{{ ansible_facts.default_ipv6.interface }}"
      input_dports:
        tcp:
          - ssh
        udp:
          - mdns
      redirect:
        tcp:
          - from: http
            to: 8080
          - from: https
            to: 8443
```
Note that the DNAT must also be enabled using `dstnat: True`.
The DNAT rules are applied in the prerouting chain for all packets entering on
any interface in the zone wher the `redirect` dictionary appears.
The input chain will accept all packets that were modified by the DNAT rule.

Forwarding from one zone to another can be configured using the `forwarding`
key in every zone. Masquerading (source NAT) may also be optionally enabled.
The following configuration allows traffic forwardinng from the `vm` to the
`wan` zone and performs masquerading on any IPv4 coming in on any interface
in the `vm` zone with a source IP address range defined in RFC1918 and going
out of any interface in the `wan` zone.
Note that source NAT must also be enabled by setting `srcnat: True`.
```yaml
firewall:
  srcnat: True
  dstnat: False
  zones:
    wan:
      interfaces:
        - wlan0
      input_dports:
        tcp:
          - ssh
    vm:
      interfaces:
        - virbr1
      input_dports:
        udp:
          - bootps

  forwarding:
    - source: vm
      destination: wan
      masquerade: True
```

User rules
----------

In order to make the role more flexible, the user can provide nftables
configuration templates to add their own rules into the `user_input`,
`forward_{{ source }}_{{ destination }}`, `zone_output`, chains as well
as define variables in `nftables.conf`.

The role looks for templates in `templates/{{ inventory_hostname }}/nftables/user_{ {{ zone }}_input, forward_{{ source }}_{{ destination }}, {{ zone }}_output, srcnat, dstnat, define}.j2`
Check `templates/nftables.conf.j2` to understand where rules from different templates get added in the firewall.

Dependencies
------------

None.

License
-------

BSD
